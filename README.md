# Graph

Module de visualisation de données sous forme graphique pour Omeka Classic

# Dépendances

Ce plugin nécessite les plugins suivants :

- [ItemRelations](https://gitlab.com/eman8/ItemRelations) (plugin Omeka mais avec le fork que nous avons développé)
- [FileRelations](https://gitlab.com/eman8/FileRelations)
- [CollectionRelations](https://gitlab.com/eman8/CollectionRelations)

# Présentation

Le plugin crée automatiquement des pages pour tous les graphes : 

- graphitem/xxx : Graphe des relations de l'item xxx
- graphcollection/xxx : Graphe des relations au sein de la collection xxx
- graphall : Graphe des relations de l'ensemble des données (attention si vous avez beaucoup de relations)
- graph/choix : page permettant de créer des graphes personnalisés

Un lien devrait apparaître sur les pages item (*items/show*) et collection (*collections/show*) menant vers les graphes correspondants.

# Credits

Ce plugin utilise la librairie Vis.js (http://visjs.org/).

Version 0.2 

Plugin réalisé pour la plate-forme EMAN (ENS-CNRS-Sorbonne nouvelle) par Vincent Buard (Numerizen) avec le soutien du consortium Cahier : https://cahier.hypotheses.org/.
Nouvelle version réalisée avec le soutien de l’IRIHS – Université de Rouen Normandie : https://irihs.univ-rouen.fr/.

Voir les explications sur le site [EMAN](https://eman-archives.org/EMAN/graph)

